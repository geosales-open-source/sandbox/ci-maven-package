# CI Maven Package

Objetivo do projeto é documentar o aprendizado de como usar ferramentas do Gitlab  
para subir Packages Maven capazes de fazer *Automatic Deployment*.

## Create Maven Project
```
mvn archetype:generate -DgroupId=com.geosales.opensorce -DartifactId=ci-maven-package -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

## Build Maven Project

**maven-archetype-quickstart** não iniciou com o compilaor maven numa versão atualizada,  
por isso foi necessário adicionar ao `pom.xml`   
```xml
  <properties>
     <maven.compiler.source>1.8</maven.compiler.source>
     <maven.compiler.target>1.8</maven.compiler.target>
  </properties>
```

## Execute Maven Project 

Para executar o projeto direto na CLI usando Maven depois do *build*.  
```bash
mvn exec:java -Dexec.mainClass="com.geosales.opensorce.App"
```

## Generate executable .jar with maven

`maven-archtype-quickstart` não me dorneceu condições de gerar um jar executável.  
Para resolver esse problema foi preciso adicionar *plugins* ao `pom.xml`.  

```xml
<build>
<plugins>
  <plugin>
    <!-- Build an executable JAR -->
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <version>3.1.0</version>
    <configuration>
      <archive>
        <manifest>
          <addClasspath>true</addClasspath>
          <classpathPrefix>lib/</classpathPrefix>
          <mainClass>com.geosales.opensorce.App</mainClass>
        </manifest>
      </archive>
    </configuration>
  </plugin>
</plugins>
</build>
```

Depois basta fazer o build novamente e executar o jar:
```bash
mvn package
java -jar target/ci-maven-package-1.0-SNAPSHOT.jar
```
> Hello World!
